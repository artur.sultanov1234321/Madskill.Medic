//
//  ViewModel.swift
//  Madskill.Medic
//
//  Created by bnkwsr2 on 14.03.2023.
//

import Alamofire
import Foundation
import UIKit

class ViewModel: ObservableObject{
    @Published var api = "https://medic.madskill.ru/api/"
    @Published var email = "artur.sultanov1234321@gmail.com"
    @Published var code = "123"
    @Published var error1 = "123"
    @Published var error = "123"
    @Published var token = ""
    //Profile
    @Published var firstname = ""
    @Published var middlename = ""
    @Published var lastname = ""
    @Published var bith = ""
    @Published var pol = ""
    @Published var id = 0
    @Published var allnews:[News] = []
    @Published var allcatalog:[Catalog] = []
    func postLogin(success:@escaping() -> Void, error:@escaping() -> Void){
        
        error1 = ""
        let emailReglax = "[a-z._0-9]+@[a-z0-9]+\\.[a-z]{1,3}"
        let emailPregar = NSPredicate(format: "SELF MATCHES %@", emailReglax)
        var isInvalidEmail = emailPregar.evaluate(with: email)
        if (!isInvalidEmail){
            error1 = "Не верно введен почтовый адрес"
            error()
            return()
        }
        var headers = HTTPHeaders()
        headers.add(HTTPHeader(name: "email", value: "\(email)"))
        AF.request("\(api)sendCode", method: .post, headers: headers)
            .responseDecodable(of:Email.self){res in
                debugPrint(res)
                success()
            }
        
    }
    func postLoginCode(success: @escaping () -> Void, error: @escaping () -> Void){
        error1 = "Неверный код"
        var headers = HTTPHeaders()
        headers.add(HTTPHeader(name: "email", value: "\(UserDefaults.standard.string(forKey: "email")!)"))
        headers.add(HTTPHeader(name: "code", value: "\(code)"))
        AF.request("\(api)signin",method: .post, headers: headers)
            .responseDecodable(of: Token.self){res in
                self.token = res.value?.token ?? ""
                if (self.token != "nil"){
                    success()
                }else{
                    error()
                }
                
            }
    }
    func getNews(success: @escaping () -> Void){
        AF.request("\(api)news", method: .get)
            .responseDecodable(of: [News].self){res in
                self.allnews = res.value ?? []
                debugPrint(res)
                success()
            }
    }
    func getCatalog(success:@escaping () -> Void){
        AF.request("\(api)catalog",method: .get)
            .responseDecodable(of: [Catalog].self){res in
                self.allcatalog = res.value ?? []
                debugPrint(res)
                success()
            }
    }
    
}
struct Email:Codable{
    let email:String
}
struct Token:Codable{
    let token:String
}
struct News:Codable{
    let id: Int
    let name: String
    let description:String
    let price:String
    let image:String
}
struct Catalog:Codable{
    let id:Int
    let name: String
    let description: String
    let price: String
    let category: String
    let time_result:String
    let preparation:String
    let bio: String
}
