//
//  OnBoard1.swift
//  Madskill.Medic
//
//  Created by bnkwsr2 on 14.03.2023.
//

import SwiftUI

struct OnBoard1: View {
    @State var onb = "Пропустить"
    @Binding var screen: String
    var body: some View {
        VStack{
        VStack{
            HStack{
                VStack{
                Button {
                    if onb=="Завершить"{
                        screen="Login"
                        debugPrint(UIScreen.main.bounds.width)
                    }
                } label: {
                    Text(onb)
                        .padding(.leading, 20)
                        .font(.custom("Lato-Regular", size: 20))
                }
                    Spacer()
                }
                Spacer()
                VStack{
                    Image("onboard")
                    Spacer()
                }
            }
        }
            TabView{
        VStack{
            HStack{
                Text("Анализы")
                    .foregroundColor(.green)
                    .font(.custom("Lato-Regular", size: 20))
                    .padding(.top, 60)
            }
            HStack{
                Text("Экспресс сбор и получение проб")
                    .font(.custom(" SFProDisplay-Regular", size: 14))
                    .foregroundColor(.gray)
                    .padding(.top, 20)
            }
            HStack{
                Image("onboard1")
                    .padding(.top, 69)
                    .padding(.bottom, 69)
            }
            Spacer()
        }
        .onAppear{
            onb = "Пропустить"
        }
                VStack{
                    HStack{
                        Text("Уведомления")
                            .font(.custom("Lato-Regular", size: 20))
                            .foregroundColor(.green)
                            .padding(.top, 60)
                    }
                    HStack{
                        Text("Вы быстро узнаете о результатах")
                            .font(.custom(" SFProDisplay-Regular", size: 14))
                            .foregroundColor(.gray)
                            .padding(.top, 20)
                    }
                    HStack{
                        Image("onboard2")
                            .padding(.top, 69)
                            .padding(.bottom, 69)
                    }
                    Spacer()
                }
                .onAppear{
                    onb = "Пропустить"
                }
                VStack{
                    HStack{
                        Text("Мониторинг")
                            .font(.custom("Lato-Regular", size: 20))
                            .foregroundColor(.green)
                            .padding(.top, 60)
                    }
                    HStack{
                        Text("Наши врачи всегда наблюдают\nза вашими показателями здоровья")
                            .font(.custom(" SFProDisplay-Regular", size: 14))
                            .padding()
                            .multilineTextAlignment(.center)
                            .lineLimit(3)
                            .foregroundColor(.gray)
                    }.padding(.top,20)
                    HStack{
                        Image("onboard3")
                            .padding(.top,69)
                            .padding(.bottom, 69)
                    }
                    Spacer()
                }.onAppear{
                    onb = "Завершить"
                }
            }.tabViewStyle(PageTabViewStyle())
                .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/1.5)
        Spacer()
            Spacer()
    }
    }
}

