//
//  Zastavka.swift
//  Madskill.Medic
//
//  Created by bnkwsr2 on 14.03.2023.
//

import SwiftUI

struct Zastavka: View {
    @Binding var screen:String
    var body: some View {
        VStack{
            ZStack{
            Image("bg")
            Image("logo")
            }
        }
        .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        .background(LinearGradient(colors: [.cyan, .blue, .cyan], startPoint: .bottomLeading, endPoint: .topTrailing))
        .onAppear{
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                screen="Login"
            }
        }
    }
    
}

