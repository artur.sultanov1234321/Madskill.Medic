//
//  Madskill_MedicApp.swift
//  Madskill.Medic
//
//  Created by bnkwsr2 on 14.03.2023.
//

import SwiftUI

@main
struct Madskill_MedicApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
