//
//  NewProfile.swift
//  Madskill.Medic
//
//  Created by bnkwsr2 on 15.03.2023.
//

import SwiftUI

struct NewProfile: View {
    @StateObject var viewmodel = ViewModel()
    @Binding var screen: String
    var body: some View {
        VStack{
            VStack{
                HStack{
                    Text("Создание карты\nпациента")
                        .font(.custom("SFProDisplay-Heavy", fixedSize: 24)).padding(.leading,20)
                    Spacer()
                    Button {
                        screen="MainScreen"
                    } label: {
                        Text("Пропустить")
                            .font(.custom("SFProDisplay-Regular", fixedSize: 15))
                            
                    }.padding(.trailing,20)

                }
                HStack{
                    Text("Без карты пациента вы не сможете заказать анализы.") .font(.custom("SFProDisplay-Regular", fixedSize: 14))
                        .foregroundColor(.gray)
                        .padding(.leading, 20)
                        .padding(.top,10)
                    Spacer()
                }
                HStack{
                    Text("В картах пациентов будут храниться результаты анализов вас и ваших близких.")
                        .font(.custom("SFProDisplay-Regular", fixedSize: 14))
                        .foregroundColor(.gray)
                        .padding(.leading,20)
                        .padding(.top,1)
                    Spacer()
                }
            }
            VStack{
                TextField("", text: $viewmodel.firstname, prompt: Text("Имя"))
                    .padding(.leading,14)
                    .frame(width: UIScreen.main.bounds.width/1.114, height: 48)
                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("lightGray")))
                    .overlay {
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(viewmodel.firstname.isEmpty ? Color("strokeColor"): .gray)
                            
                    }
            }.padding(.top,32)
            VStack{
                TextField("", text: $viewmodel.middlename, prompt: Text("Отчество"))
                    .padding(.leading,14)
                    .frame(width: UIScreen.main.bounds.width/1.114, height: 48)
                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("lightGray")))
                    .overlay {
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(viewmodel.middlename.isEmpty ? Color("strokeColor"): .gray)
                            
                    }
            }.padding(.top,20)
            VStack{
                TextField("", text: $viewmodel.lastname, prompt: Text("Фамилия"))
                    .padding(.leading,14)
                    .frame(width: UIScreen.main.bounds.width/1.114, height: 48)
                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("lightGray")))
                    .overlay {
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(viewmodel.lastname.isEmpty ? Color("strokeColor"): .gray)
                            
                    }
            }.padding(.top,20)
            VStack{
                TextField("", text: $viewmodel.bith, prompt: Text("Дата рождения"))
                    .padding(.leading,14)
                    .frame(width: UIScreen.main.bounds.width/1.114, height: 48)
                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("lightGray")))
                    .overlay {
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(viewmodel.bith.isEmpty ? Color("strokeColor"): .gray)
                            
                    }
            }.padding(.top,20)
            VStack{
            Button {
                if (viewmodel.pol == ""){
                    viewmodel.pol = "Мужской"
                }else if (viewmodel.pol == "Мужской"){
                    viewmodel.pol = "Женский"
                }else if (viewmodel.pol == "Женский"){
                    viewmodel.pol = ""
                }
            } label: {
                HStack{
                if viewmodel.pol == ""{
                    Text("Пол")
                        .foregroundColor(Color("textFieldColor"))
                        .padding(.leading,14)
                        
                }else{
                    Text("\(viewmodel.pol)")
                        .foregroundColor(.black)
                        .padding(.leading,14)
                }
                    Spacer()
                    Image(systemName: "chevron.down")
                        .foregroundColor(.gray)
                        .font(.custom("SFProDisplay-Heavy", fixedSize: 16))
                        .padding(.trailing,19)
                }.frame(width: UIScreen.main.bounds.width/1.114, height: 48)
                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("lightGray")))
                    .overlay {
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(viewmodel.pol.isEmpty ? Color("strokeColor"): .gray)
                    }
                
            }
            }.padding(.top,20)
            VStack{
                if (viewmodel.firstname == "" || viewmodel.middlename == "" || viewmodel.lastname == "" || viewmodel.bith == "" || viewmodel.pol == ""){
                    Text("Создать")
                        .foregroundColor(.white)
                        .frame(width: UIScreen.main.bounds.width/1.114, height: 56)
                        .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("butNoAct")))
                }else{
                    Button {
                        viewmodel.id = UserDefaults.standard.integer(forKey: "userID")
                        UserDefaults.standard.set(viewmodel.firstname, forKey: "firstname\(viewmodel.id)")
                        UserDefaults.standard.set(viewmodel.lastname, forKey: "lastname\(viewmodel.id)")
                        UserDefaults.standard.set(viewmodel.middlename, forKey: "middlename\(viewmodel.id)")
                        UserDefaults.standard.set(viewmodel.bith, forKey: "bith\(viewmodel.id)")
                        UserDefaults.standard.set(viewmodel.pol, forKey: "pol\(viewmodel.id)")
                        UserDefaults.standard.set(viewmodel.id, forKey:"userID")
                        screen = "MainScreen"
                    } label: {
                        Text("Создать")
                            .font(.custom("SFProDisplay-Semibold", fixedSize: 17))
                            .foregroundColor(.white)
                            .frame(width: UIScreen.main.bounds.width/1.114, height: 56)
                            .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.blue))
                    }

                }
            }.padding(.top,48)
        }.ignoresSafeArea(.keyboard)
    }
}


