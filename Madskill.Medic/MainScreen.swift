//
//  MainScreen.swift
//  Madskill.Medic
//
//  Created by bnkwsr2 on 15.03.2023.
//

import SwiftUI

struct MainScreen: View {
    @State var search = ""
    @StateObject var viewmodel = ViewModel()
    @State var cataloge = "Популярные"
    @State var open = false
    var body: some View {
        ZStack{
            TabView{
                VStack{
                    if open==false{
        VStack{
            HStack{
                Image(systemName: "magnifyingglass")
                    .foregroundColor(.gray)
                    .padding(.leading,14)
                TextField("", text: $search, prompt: Text("Искать анализы"))
            }.frame(width: UIScreen.main.bounds.width/1.114, height: 48)
                .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("lightGray")))
                .overlay {
                    RoundedRectangle(cornerRadius: 10).stroke(search.isEmpty ? Color("strokeColor"):.gray)
                }
            VStack{
                HStack{
                Text("Акции и новости")
                    .foregroundColor(.gray)
                    .font(.custom("SFProDisplay-Semibold", fixedSize: 17))
                    .padding(.leading,20)
                    Spacer()
                }
                    ScrollView(.horizontal, showsIndicators: true){
                        HStack{
                        ForEach(viewmodel.allnews, id: \.id){news in
                            Button {
                                //
                            } label: {
                                VStack{
                                ZStack{
                                    AsyncImage(url: URL(string: news.image.replacingOccurrences(of: " ", with: "%20"))) { images in
                                        images
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .offset(x: 67)
                                    } placeholder: {
                                        LinearGradient(colors: [.indigo,.blue,.cyan], startPoint: .leading, endPoint: .trailing)
                                    }
                                    HStack{
                                        VStack(alignment:.leading){
                                        Text(news.name)
                                                .font(.custom("SFProDisplay-Heavy", fixedSize: 20))
                                                .foregroundColor(.white)
                                                .shadow(color: .black, radius: 1, x: 0.1, y: 0.1)
                                                .padding(.trailing, 50)
                                                .multilineTextAlignment(.leading)
                                            Text(news.description)
                                                .foregroundColor(.white)
                                                .shadow(color: .black, radius: 1, x: 0.1, y: 0.1)
                                                .font(.custom("SFProDisplay-Regular",fixedSize: 14))
                                                .multilineTextAlignment(.leading)
                                                .padding(.top, 20)
                                            Text("\(news.price) ₽")
                                                .foregroundColor(.white)
                                                .shadow(color: .black, radius: 1, x: 0.1, y: 0.1)
                                                .font(.custom("SFProDisplay-Heavy", size:20))
                                            
                                        }
                                        
                                            
                                    }

                                }
                            
                                
                            }
                            }.frame(width: 270, height: 152)
                                .background(LinearGradient(colors: [.indigo,.blue, .cyan], startPoint: .leading, endPoint: .trailing).opacity(20)
                                    )
                                .clipShape(RoundedRectangle(cornerRadius: 12))

                            
                    }
                    }
                    }.padding(.leading,20)
            }.padding(.top,32)
            VStack{
                HStack{
                    Text("Каталог анализов")
                        .foregroundColor(.gray)
                        .font(.custom("SFProDisplay-Semibold", fixedSize: 17))
                        .padding(.leading,20)
                        .padding(.top, 20)
                        Spacer()
                }
            }
        }
                    }
                VStack{
                
                    ScrollView(.horizontal){
                        HStack{
                            Button {
                                cataloge="Популярные"
                            } label: {
                                if cataloge=="Популярные"{
                                Text("Популярные")
                                    .foregroundColor(.white)
                                    .padding(.horizontal, 20)
                                    .padding(.vertical, 14)
                                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.blue))
                                }else{
                                    Text("Популярные")
                                        .foregroundColor(.gray)
                                        .padding(.horizontal, 20)
                                        .padding(.vertical, 14)
                                        .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("lightGray")))
                                }
                            }
                            Button {
                                cataloge="COVID"
                            } label: {
                                if cataloge=="COVID"{
                                Text("COVID")
                                    .foregroundColor(.white)
                                    .padding(.horizontal, 20)
                                    .padding(.vertical, 14)
                                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.blue))
                                }else{
                                    Text("COVID")
                                        .foregroundColor(.gray)
                                        .padding(.horizontal, 20)
                                        .padding(.vertical, 14)
                                        .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("lightGray")))
                                }
                            }
                            Button {
                                cataloge="Онкогенетические"
                            } label: {
                                if cataloge=="Онкогенетические"{
                                Text("Онкогенетические")
                                    .foregroundColor(.white)
                                    .padding(.horizontal, 20)
                                    .padding(.vertical, 14)
                                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.blue))
                                }else{
                                    Text("Онкогенетические")
                                        .foregroundColor(.gray)
                                        .padding(.horizontal, 20)
                                        .padding(.vertical, 14)
                                        .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("lightGray")))
                                }
                            }
                            Button {
                                cataloge="ЗОЖ"
                            } label: {
                                if cataloge=="ЗОЖ"{
                                Text("ЗОЖ")
                                    .foregroundColor(.white)
                                    .padding(.horizontal, 20)
                                    .padding(.vertical, 14)
                                    .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.blue))
                                }else{
                                    Text("ЗОЖ")
                                        .foregroundColor(.gray)
                                        .padding(.horizontal, 20)
                                        .padding(.vertical, 14)
                                        .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color("lightGray")))
                                }
                            }

                        
                        
                    }
                    }.padding(.leading,20)
                
                    ScrollView(.vertical){
                        ForEach(viewmodel.allcatalog, id:\.id){cat in
                            if cat.category == cataloge{
                                VStack{
                                    HStack{
                                        Text(cat.name)
                                            .font(.custom("SFProDisplay-Medium",size:16))
                                            .multilineTextAlignment(.leading)
                                            .padding(.leading, 14)
                                            .padding(.trailing, 32)
                                        Spacer()
                                    }
                                    HStack{
                                        VStack(alignment: .leading){
                                        Text(cat.time_result)
                                            .font(.custom("SFProDisplay-Semibold",size:14))
                                            .foregroundColor(.gray)
                                            .multilineTextAlignment(.leading)
                                            .padding(.top, 16)
                                            
                                        
                                    
                                        Text("\(cat.price) ₽")
                                            .font(.custom("SFProDisplay-Semibold",size:17))
                                            .multilineTextAlignment(.leading)
                                            .padding(.top, 1)
                                            
                                        
                                    }.padding(.leading,14)
                                        Spacer()
                                        Button {
                                            
                                        } label: {
                                            Text("Добавить")
                                                .foregroundColor(.white)
                                                .padding(.horizontal, 16)
                                                .padding(.vertical,10)
                                                .font(.custom("SFProDisplay-Semibold",size:14))
                                                .background(RoundedRectangle(cornerRadius: 10).foregroundColor(.blue))
                                                .padding(.trailing,16)
                                                .padding(.top,5)
                                        }

                                    }
                                }.frame(width: UIScreen.main.bounds.width/1.114, height: 136)
                                    .background(RoundedRectangle(cornerRadius: 10)
                                        .foregroundColor(.white)
                                        .shadow(color: .gray, radius: 1, x: 0.1, y: 0.1))
                                    .padding(.horizontal, 20)
                                    .padding(.vertical, 4)
                            }
                        }
                    }.frame(width: UIScreen.main.bounds.width)
                }.gesture(DragGesture(minimumDistance: 0.0)
                    .onChanged({ _ in
                        debugPrint("\(open)1")
                    })
                    .onEnded({ _ in
                        open.toggle()
                    }))
            
        }.tabItem {
            Image("analyses")
                .renderingMode(.template)
            Text("Анализы")
        }
                VStack{
                    Image("spotlight")
                }.tabItem {
                    Image("result")
                        .renderingMode(.template)
                    Text("Результаты")
                }
                VStack{
                    Image("spotlight")
                }.tabItem {
                    Image(systemName:"message")
                    Text("Поддержка")
                }
                VStack{
                    Image("spotlight")
                }.tabItem{
                    Image(systemName: "person")
                    
                    Text("Профиль")
                }
            }
        }.onAppear {
            viewmodel.getNews {
                //
            }
            viewmodel.getCatalog {
                //
            }
        }
    }
}

