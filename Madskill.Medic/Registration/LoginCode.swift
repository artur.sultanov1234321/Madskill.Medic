//
//  LoginCode.swift
//  Madskill.Medic
//
//  Created by bnkwsr2 on 14.03.2023.
//

import SwiftUI
import CITPincode

struct LoginCode: View {
    @StateObject var viewmodel = ViewModel()
    @State var forceCooldownOnce: Bool = false
    @State private var error: String?
    @State var code = ""
    @State var time = 60
    @Binding var screen:String
    @State var alert = false
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    var body: some View {
        VStack{
            HStack{
                Button {
                    screen="Login"
                } label: {
                    Image(systemName: "chevron.left")
                        .foregroundColor(.gray)
                        .padding(12)
                        .background(RoundedRectangle(cornerRadius: 10)
                            .foregroundColor(Color("lightGray")))
                }

                
                Spacer()
            }.padding(.leading, 20)
                .padding(.top, 40)
            Spacer()
            VStack{
            Text("Введите код из E-mail")
                    .foregroundColor(.black)
                    .font(.custom("SFProDisplay-Semibold", size: 17))
            }
            VStack{
                CITPincodeView(code: $code, error: $error, forceCooldownOnce: $forceCooldownOnce, config: .init(codeLength: 4, font: .custom("SFProDisplay-Regular", fixedSize: 20), placeholder: "", backgroundColor: Color("lightGray"), selectedBorderColor: Color("lightGray"), cellSize: CGSize(width: 46, height: 46), cellCornerRadius: 10, keyboardType: .numberPad), onEnteredCode: {
                    sendCode()
                }, onResendCode: {
                    resendCode()
                })
                .onAppear{
                    forceCooldownOnce=true
                }
            }
            VStack{
                if (forceCooldownOnce==false){
                    Text("Код будет отправлен повторно")
                        .foregroundColor(.gray)
                        .font(.custom("SFProDisplay-Regular", fixedSize: 15))
                        .multilineTextAlignment(.center)

                }else{
                    Text("Отправить код повторно можно\n будет через \(time) секунд")
                        .foregroundColor(.gray)
                        .font(.custom("SFProDisplay-Regular", fixedSize: 15))
                        .multilineTextAlignment(.center)
                        .onReceive(timer) { _ in
                            if forceCooldownOnce==true{
                                if time>0{
                                    time-=1
                                }else if time==0{
                                    forceCooldownOnce=false
                                    resendCode()
                                    
                                }
                            }
                        }
                }
            }
            Spacer()
            Spacer()
        }
    }
    private func sendCode() {
        viewmodel.postLoginCode {
            screen = "Pincode"
        } error: {
            alert = true
        }

    }
    private func resendCode() {
        viewmodel.email = UserDefaults.standard.string(forKey: "email")!
        viewmodel.postLogin {
            forceCooldownOnce=true
            time = 60
        }error: {
            alert=true
        }
    }
}
