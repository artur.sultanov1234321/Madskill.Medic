//
//  Login.swift
//  Madskill.Medic
//
//  Created by bnkwsr2 on 14.03.2023.
//

import SwiftUI

struct Login: View {
    @Binding var screen: String
    @StateObject var viewmodel = ViewModel()
    @State var alert = false
    var body: some View {
        VStack{
            VStack{
                HStack{
                    Image("hello")
                        .padding(.leading, 20)
                        
                    Text("Добро пожаловать!")
                        .font(.custom("SFProDisplay-Heavy", size: 24))
                    Spacer()
                }.padding(.top, 77)
                HStack{
                    Text("Войдите, чтобы пользоваться функциями\nприложения")
                        .padding(.leading, 20)
                        .padding(.top, 15)
                        .font(.custom("SFProDisplay-Regular", size: 15))
                    Spacer()
                }
                
                HStack{
                    Text("Вход по E-mail")
                        .padding(.top,64)
                        .padding(.leading, 20)
                        .font(.custom("SFProDisplay-Regular", size: 14))
                        .foregroundColor(.gray)
                    Spacer()
                }
                HStack{
                    TextField("", text: $viewmodel.email, prompt: Text("example@mail.ru"))
                        .font(.custom("SFProDisplay-Regular", size: 15))
                        .frame(width: 335, height: 48)
                        .background(RoundedRectangle(cornerRadius: 10)
                            .frame(width: UIScreen.main.bounds.width/1.114, height: 48)
                            .foregroundColor(Color("lightGray")))
                        .keyboardType(.emailAddress)
                }
                HStack{
                    if viewmodel.email==""{
                        Text("Далее")
                            .foregroundColor(.white)
                            .font(.custom("SFProDisplay-Semibold", size: 17))
                            .background(RoundedRectangle(cornerRadius: 10)
                                .frame(width: UIScreen.main.bounds.width/1.114, height: 56)
                                .foregroundColor(Color("butNoAct")))
                    }else{
                        Button {
                            viewmodel.postLogin{
                                UserDefaults.standard.set(viewmodel.email, forKey: "email")
                                screen="LoginCode"
                            } error: {
                                debugPrint(viewmodel.error1)
                                alert=true
                            }
                        } label: {
                            Text("Далее")
                                .foregroundColor(.white)
                                .font(.custom("SFProDisplay-Semibold", size: 17))
                                .background(RoundedRectangle(cornerRadius: 10)
                                    .frame(width: UIScreen.main.bounds.width/1.114, height: 56))
                        }
                    }

                }.padding(.top, 48)
                Spacer()
                HStack{
                Text("Или войдите с помощью")
                    .foregroundColor(.gray)
                    .font(.custom("SFProDisplay-Regular", size: 15))
                    .frame(width:UIScreen.main.bounds.width/1.114)
                }
                HStack{
                    Text("Войти с Яндекс")
                        .foregroundColor(.black)
                        .font(.custom("SFProDisplay-Medium", size: 17))
                        .overlay {
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color("lightGray"))
                                .frame(width: UIScreen.main.bounds.width/1.114, height: 60)
                                
                        }
                }.padding(.top, 34)
                    .padding(.bottom, 34)
            }
        }
        .alert("\(viewmodel.error1)", isPresented: $alert) {
            //
        }
    }
}

