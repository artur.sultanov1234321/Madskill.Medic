//
//  Pincode.swift
//  Madskill.Medic
//
//  Created by bnkwsr2 on 14.03.2023.
//

import SwiftUI

struct Pincode: View {
    @State var pressing: [Bool] = []
    @State var count = 0
    @State private var pin = ""
    @Binding var screen:String
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Button {
                    screen="NewProfile"
                } label: {
                    Text("Пропустить")
                        .font(.custom("SFProDisplay-Regular", fixedSize: 15))
                        .padding(.trailing, 20)
                        .padding(.top, 20)
                }

            }
            VStack{
            HStack{
                Text("Создайте пароль")
                    .font(.custom("SFProDisplay-Heavy", fixedSize: 24))
            }
            HStack{
                Text("Для защиты ваших персональных данных")
                    .foregroundColor(.gray)
                    .font(.custom("SFProDisplay-Regular", fixedSize: 15))
                    .padding(.top, 1)
            }
            }.padding(.top,40)
            Spacer()
            HStack{
                if count>0{
                    RoundedRectangle(cornerRadius: 90)
                        .frame(width: 16, height: 16)
                        .foregroundColor(.blue)
                }else{
                    RoundedRectangle(cornerRadius: 90)
                        .stroke(.blue)
                        .frame(width: 16, height: 16)
                }
                if count>1{
                    RoundedRectangle(cornerRadius: 90)
                        .frame(width: 16, height: 16)
                        .foregroundColor(.blue)
                }else{
                    RoundedRectangle(cornerRadius: 90)
                        .stroke(.blue)
                        .frame(width: 16, height: 16)
                }
                if count>2{
                    RoundedRectangle(cornerRadius: 90)
                        .frame(width: 16, height: 16)
                        .foregroundColor(.blue)
                }else{
                    RoundedRectangle(cornerRadius: 90)
                        .stroke(.blue)
                        .frame(width: 16, height: 16)
                }
                if count==4{
                    RoundedRectangle(cornerRadius: 90)
                        .frame(width: 16, height: 16)
                        .foregroundColor(.blue)
                }else{
                    RoundedRectangle(cornerRadius: 90)
                        .stroke(.blue)
                        .frame(width: 16, height: 16)
                }
            }
            Spacer()
            HStack{
            Button {
                if count<4{
                count+=1
                pin += "1"
                    if count==4{
                        UserDefaults.standard.set(pin, forKey: "pin")
                        screen="NewProfile"
                    }
                }
            } label: {
                Text("1")
                    .font(.custom("SFProDisplay-Semibold", fixedSize: 24))
                    
                        
            }.buttonStyle(hh())
            Button {
                if count<4{
                count+=1
                pin += "2"
                    if count==4{
                        UserDefaults.standard.set(pin, forKey: "pin")
                        screen="NewProfile"
                    }
                }
            } label: {
                Text("2")
                    .font(.custom("SFProDisplay-Semibold", size: 24))
                    
                        
            }
            .buttonStyle(hh())
            Button {
                if count<4{
                count+=1
                pin += "3"
                    if count==4{
                        UserDefaults.standard.set(pin, forKey: "pin")
                        screen="NewProfile"
                    }
                }
            } label: {
                Text("3")
                    .font(.custom("SFProDisplay-Semibold", size: 24))
                    
                        
            }
            .buttonStyle(hh())
            }
            HStack{
            Button {
                if count<4{
                count+=1
                pin += "4"
                    if count==4{
                        UserDefaults.standard.set(pin, forKey: "pin")
                        screen="NewProfile"
                    }
                }
            } label: {
                Text("4")
                    .font(.custom("SFProDisplay-Semibold", fixedSize: 24))
                    
                        
            }.buttonStyle(hh())
            Button {
                if count<4{
                count+=1
                pin += "5"
                    if count==4{
                        UserDefaults.standard.set(pin, forKey: "pin")
                        screen="NewProfile"
                    }
                }
            } label: {
                Text("5")
                    .font(.custom("SFProDisplay-Semibold", size: 24))
                    
                        
            }
            .buttonStyle(hh())
            Button {
                if count<4{
                count+=1
                pin += "6"
                    if count==4{
                        UserDefaults.standard.set(pin, forKey: "pin")
                        screen="NewProfile"
                    }
                }
            } label: {
                Text("6")
                    .font(.custom("SFProDisplay-Semibold", size: 24))
                    
                        
            }
            .buttonStyle(hh())
            }
            HStack{
            Button {
                if count<4{
                count+=1
                pin += "7"
                    if count==4{
                        UserDefaults.standard.set(pin, forKey: "pin")
                        screen="NewProfile"
                    }
                }
            } label: {
                Text("7")
                    .font(.custom("SFProDisplay-Semibold", fixedSize: 24))
                    
                        
            }.buttonStyle(hh())
            Button {
                if count<4{
                count+=1
                pin += "8"
                    if count==4{
                        UserDefaults.standard.set(pin, forKey: "pin")
                        screen="NewProfile"
                    }
                }
            } label: {
                Text("8")
                    .font(.custom("SFProDisplay-Semibold", size: 24))
                    
                        
            }
            .buttonStyle(hh())
            Button {
                if count<4{
                count+=1
                pin += "9"
                    if count==4{
                        UserDefaults.standard.set(pin, forKey: "pin")
                        screen="NewProfile"
                    }
                }
            } label: {
                Text("9")
                    .font(.custom("SFProDisplay-Semibold", size: 24))
                    
                        
            }
            .buttonStyle(hh())
            }
            HStack{
                VStack{
                    
                }.frame(width: 80, height: 80)
            Button {
                count+=1
                pin += "0"
            } label: {
                Text("0")
                    .font(.custom("SFProDisplay-Semibold", size: 24))
                    
                        
            }
            .buttonStyle(hh())
            Button {
                count-=1
                pin.remove(at: pin.index(before: pin.endIndex))
            } label: {
                Image(systemName: "delete.left")
                    .resizable()
                    .foregroundColor(.black)
                    .frame(width: 35, height: 30)
                    
                        
            }.frame(width: 80, height: 80)
                
            }
            Spacer()
        }
        
    }
}

struct hh: ButtonStyle {

  func makeBody(configuration: Self.Configuration) -> some View {
    configuration.label
      .padding()
      .foregroundColor(configuration.isPressed ? .white : .black)
      .frame(width: 80, height: 80)
      .background(configuration.isPressed ? .blue : Color("lightGray"))
      .cornerRadius(100)
  }

}
