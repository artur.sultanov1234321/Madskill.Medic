//
//  ContentView.swift
//  Madskill.Medic
//
//  Created by bnkwsr2 on 14.03.2023.
//

import SwiftUI

struct ContentView: View {
    @State var screen = "NewProfile"
    var body: some View {
        VStack{
            if screen=="Zastavka"{
                Zastavka(screen: $screen)
            }else if screen=="OnBoard1"{
                OnBoard1(screen: $screen)
            }else if screen=="Login"{
                Login(screen: $screen)
            }else if screen=="LoginCode"{
                LoginCode(screen: $screen)
            }else if screen=="Pincode"{
                Pincode(screen: $screen)
            }else if screen=="NewProfile"{
                NewProfile(screen: $screen)
            }else if screen=="MainScreen"{
                MainScreen()
            }
        }
    }
}

